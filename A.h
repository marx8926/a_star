#ifndef A_H
#define A_H
#include <vector>
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

typedef struct cell
{
    float g;
    float h;
    bool activo;
    float x,y;
    bool path;
    bool usado;
};
class A
{
    public:
        A();
        virtual ~A();
        void create_matrix(int n, int m, vector<cell> obstacles);
       vector<cell> calcular_A(cell ini, cell fin, vector<vector<cell> > P);
        vector<vector<cell> > get_matrix(){return M;}
        void show(vector<vector<cell> > M);


    private:
     int n; //number of rows
     int m; //number of column
     vector<vector<cell> > M; //matrix of cells
     float heuristic(cell p1, cell p2);
     //calculate the table of heuristicsfr
     vector<vector<cell> >  fill_table(cell fin,vector<vector<cell> > P);
     //el minimo nodo fev de la lista
     int minimo(vector<cell>  M);
     //expande el nodo seleccionado
     vector<cell> expande(cell p, vector<vector<cell> > M);

     void calc_fev(vector<vector<cell> > &M, vector<cell> & L, cell el);

     //regresar_ruta
     vector<cell> ruta(vector<cell> p);




};

#endif // A_H
