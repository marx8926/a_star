#include "A.h"


A::A()
{
    //ctor
}

A::~A()
{
    //dtor
}


//create a matrix
void A::create_matrix(int ns, int ms, vector<cell> o)
{
    cell temp;

    temp.activo = true;
    temp.path = false;
    temp.usado = false;
    temp.g = 0;
    temp.h = 0;

    M.clear();

    for(int i =0;i <ns; i++)
    {
        vector<cell> t;
        for(int j=0; j<ms; j++)
        {
            temp.x = i;
            temp.y = j;

            t.push_back(temp);

        }

        M.push_back(t);
    }

    int p = o.size();

    for(int i=0;i<p;i++)
    {
        M[o[i].x][o[i].y].activo = false;
        M[o[i].x][o[i].y].path = false;
    }

    this->n = ns;
    this->m = ms;
}
vector<cell>  A::calcular_A(cell ini, cell fin, vector<vector<cell> > P)
{
      vector<cell> select;
      vector<cell> posibles;
      vector<cell> path;

       int idx;
      ini.usado = true;
      ini.path = true;
      select.push_back(ini);

      cell temp;

      P = this->fill_table(fin,P);




      // meta = ini

      if(ini.x == fin.x && ini.y == fin.y)
      {
        P[ini.x][ini.y].path= true;
        P[ini.x][ini.y].usado = true;

        path.push_back(ini);

        return path;
      }

      while(!select.empty())
      {
         //obtenemos el minimo elmento de la lista
         idx = this->minimo(select);
         temp = select[idx];

         //eliminaos el elemento de la lista
         select.erase(select.begin()+idx);
         P[temp.x][temp.y].usado = true;

         posibles.push_back(temp);

        // si el minimo es nodo meta
         if(temp.x == fin.x && temp.y == fin.y)
         {
            P[temp.x][temp.y].path = true;

            path = this->ruta(posibles);

            return path;
         }

         //expando los nodos seleccionados
         vector<cell> ex = expande(temp, P);


         //calcular el fev para cada sucesor
         calc_fev(P, ex, temp);

         //insertar cada sucesor en la lista eliminando nodos redundantes
         select.insert(select.end(),ex.begin(),ex.end());

      }

      return path;

}

vector<vector<cell> >  A::fill_table(cell fin, vector<vector<cell> >  P)
{

    for(int i =0 ; i< n;i++)
    {
        for(int j=0; j<m; j++)
        {
           if(P[i][j].activo)
                P[i][j].h = heuristic(P[i][j], fin);
        }
    }

    return P;
}

float A::heuristic(cell p1, cell p2)
{
    float r = fabs(p1.x - p2.x) + fabs(p1.y - p2.y);

    return r;
}
int A::minimo(vector<cell>  lista)
{
    vector<float> lista_f;
    //calculamos las fev e insertamos en un vector
    for(int i=0; i< lista.size(); i++)
    {
        lista_f.push_back(lista[i].g+lista[i].h);
    }
// el minimo elemento de la lista de fevs
    int idx = min_element(lista_f.begin(),lista_f.end())-lista_f.begin();

    return idx;
}

vector<cell> A::expande(cell p, vector<vector<cell> > M)
{

    vector<cell> lista;
    cell temp;

    int i = p.x;
    int j = p.y;

     //norte
    if( (i-1)>=0 && M[(i-1)][j].activo == true && M[(i-1)][j].usado== false)
    {
        temp = M[(i-1)][j];
        //agregar si no se explora
        if(temp.g == 0)
            lista.push_back(temp);
    }

    //sur
    if((i+1)< n && M[(i+1)][j].activo == true && M[(i+1)][j].usado == false)
    {
        temp = M[(i+1)][j];
        //agregar si no se explora
        if(temp.g == 0)
            lista.push_back(temp);
    }

    //oeste
    if((j-1)>=0 && M[i][j-1].activo == true && M[i][j-1].usado == false)
    {
        temp = M[i][j-1];
        //agregar si no se explora
        if(temp.g == 0)
            lista.push_back(temp);
    }

    //este
    if((j+1)<m && M[i][j+1].activo == true && M[i][j+1].usado == false)
    {
        temp = M[i][j+1];
        //agregar si no se explora
        if(temp.g == 0)
            lista.push_back(temp);
    }

    return lista;
}

void A::calc_fev(vector<vector<cell> > &M, vector<cell> & L, cell el)
{

    int n = L.size();
    cell temp ;

    for(int i=0; i< n; i++)
    {
        temp = L[i];
        temp.g = el.g + 1;

        L[i] = temp;
        M[temp.x][temp.y] = temp;
    }
}


void A::show(vector<vector<cell> > M)
{
    int n = M.size();
    for(int i=0; i< n; i++)
    {
        for(int j =0; j< M[i].size(); j++)
        {
            if(!M[i][j].activo)
             {
               cout<<" X ";
               continue;
               }


            if(M[i][j].path)
               cout<<" P ";
            else
                cout<<" . ";
        }
        cout<<endl;
    }
}

vector<cell> A::ruta(vector<cell> p)
{
    vector<cell> path;
   cell temp;
   cell nueva;

   int i = p.size() - 2;

   path.push_back(p.back());
   temp = p.back();

   while(i >=0)
   {
       //solo agregar verticales y horizantales

       nueva = p[i];

       if(
           ( (temp.x == nueva.x) && ( (nueva.y == (temp.y -1) ) || (nueva.y == (temp.y +1)))  ) ||
           ( (temp.y == nueva.y) && ( (nueva.x == (temp.x -1) ) || (nueva.x == (temp.x +1)))  )
               )
       {
           temp = nueva;
           path.push_back(nueva);

       }
       --i;
   }

    return path;
}
