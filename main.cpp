#include <iostream>
#include "A.h"
using namespace std;

int main()
{
    A a;

    //creamos obstaculos
    cell temp;
    temp.activo = false;
    temp.path = false;
    temp.usado = false;

    vector<cell> obs; //obstaculos

    int px[] = {2,2,3,4,4,4};
    int py[] = {4,5,4,2,3,4};

    for(int k=0; k< 6; k++)
    {
        temp.x = px[k]; temp.y = py[k];
       obs.push_back(temp);
    }



    a.create_matrix(6,6,obs);
    cout<<"antes\n";
    vector<vector<cell> > R = a.get_matrix();
    a.show(R);

    cell ini, fin;

    ini = R[4][1];
    fin = R[1][5];


    ini.path = ini.usado = true;
    fin.path = fin.usado = true;
    R[4][1] = ini;
   // R[1][5] = fin;

    a.calcular_A(ini,fin,R);

    return 0;
}
